package main

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"log"
)

type Game struct {
	ebiten.Game
}

func (g *Game) Update(_ *ebiten.Image) error {
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	_ = screen.Fill(color.RGBA{R: 0xff, A: 0xff})
}

func (g *Game) Layout(_, _ int) (int, int) {
	return 320, 240
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Fill")
	if err := ebiten.RunGame(new(Game)); err != nil {
		log.Fatalln(err)
	}
}
