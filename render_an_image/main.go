package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	_ "image/png"
	"log"
)

var img *ebiten.Image

func init() {
	var err error
	if img, _, err = ebitenutil.NewImageFromFile("png/gopher.png", ebiten.FilterDefault); err != nil {
		log.Fatalln(err)
	}
}

type Game struct {
	ebiten.Game
}

func (g *Game) Update(_ *ebiten.Image) error {
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	_ = screen.DrawImage(img, nil)
}

func (g *Game) Layout(_, _ int) (int, int) {
	return 640, 480
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Render an image")
	if err := ebiten.RunGame(new(Game)); err != nil {
		log.Fatalln(err)
	}
}
